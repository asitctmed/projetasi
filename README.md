# Projet ASI

## Noms
- Thomas CORDIER
- Etienne DUROUSSET
- Thomas MOLLARET

## Repo GIT
https://gitlab.com/asitctmed/projetasi.git

## Vidéos
- ReactJS : https://youtu.be/jMAYP_SMkZo
- NodeJS (Socket.io)  : https://youtu.be/Ktlqukz0PAQ
- JEE  : https://www.youtube.com/watch?v=-nnwuAVW2vk

## Lancement

- NodeJS : 
    - changer chemin de config.json
    - npm start dans NodeJS\tp_starter
- ReactJs : 
    - npm start dans ReactJS

## Réalisés

### ReactJS
- Affichage du slid selectionné
- Modification de slid
- Modification d'un contenu de slid via drag and drop depuis la content map
- Ajout de contenu dans la content Map via le bouton "AddContent"
- Récupération de la présentation et de la content map depuis le serveur ExpressJS
- Ajout/Suppresion de slide

### NodeJS
- Méthodes GET/POST sur les présentations
- Méthodes GET/POST/PUT/DELETE sur les contenus
    - Affichage d'un contenu suivant son type quand un GET est effectué
    - Gestion de l'upload d'un fichier lors d'un POST de contenu de type image
    - Génération de l'UUID lors du POST d'un contenu
- Tests unitaires validés
- Socket.io implémenté avec les différents événements requis
- Tests de l'implémentation de socket.io avec une page admin et plusieurs page watcher (cf vidéo)

### JEE
- Webservice mise en place et fonctionnel
- EJB sender : OK
- EJB receiver : OK
- EJB message driven : OK
- DataContainer : OK (classe contenant des données en dur pour faire fonctionner l'ensemble sans BDD )
- BDD: OK (table avec des données mise en place)
- DAO est présent pour le lien avec une BDD

## Non réalisés

### React JS
- Modification du titre et de la description de la présentation
- Ajout d'un fichier dans la content map via drag and drop
- Sauvegarde de la présentation vers ExpressJS
- Création de watchers
- Lancement d'une présentation avec navigation des slides
- Authentification

### NodeJS
- Gestion de socket.io côté ReactJS
- Webservice "/login" connecté au J2E
- Permettre la diffusion de plusieurs présentations en même temps

### JEE
- Problème avec le DAO
- Remarque : La partie JEE fonctionne, seul le lien avec une BDD est manquant. Un DAO a été mis en place mais celui-ci ne fonctionne pas.
Nous avons diagnostiqué le problème au niveau de la requête à la base.
