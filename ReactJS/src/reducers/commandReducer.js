import { COMMAND_PRESENTATION } from '../actions/commandAction'

const defaultState = {
    cmdPres: '',
}

export default (state = defaultState, action) => {
    switch (action.type) {
        case COMMAND_PRESENTATION: {
            return {
                ...state,
                cmdPres: action.obj
            }
        }
        default: return state
    }
}
