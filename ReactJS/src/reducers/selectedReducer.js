const selectedReducer = (state = { slid: {}, content_id: null}, action) => {
    switch (action.type) {
        case 'UPDATE_SELECTED_SLID':
        return {
            ...state,
            slid: action.obj
        }
        case 'UPDATE_DRAGGED_ELT':
        return {
            ...state,
            content_id: action.obj
        }
        default:
            return state;
    }
}
export default selectedReducer;