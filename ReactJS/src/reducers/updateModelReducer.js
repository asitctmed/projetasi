import Tools from '../services/tools.js';

const updateModelReducer = (state = { pres: {}, contentMap: {}, }, action) => {
    switch (action.type) {
        case 'UPDATE_PRESENTATION':
            return {
                ...state,
                pres: action.obj,
            }
        case 'UPDATE_PRESENTATION_SLIDS':
            const nstate = {
                ...state,
                pres: updateSlidOfPres(state.pres, action.obj),
            }
            return (nstate);
        case 'DELETE_SLID': {
            const slideId = action.obj;
            const slidArrayOld = state.pres.slidArray;
            return {
                ...state,
                pres: {
                    ...state.pres,
                    slidArray: slidArrayOld.filter(slide => slide.id !== slideId)
                },
            };
        }
        case 'UPDATE_CONTENT_MAP':
            return {
                ...state,
                contentMap: action.obj,
            }

        case 'ADD_CONTENT': {
            const content = action.obj;
            const id = Tools.generateUUID()
            return {
                ...state,
                contentMap: [
                    ...state.contentMap,
                    {
                        ...content,
                        id
                    }
                ],
            };
        }

        default:
            return state;
    }
}

const updateSlidOfPres = (pres, slide) => {
    for (let i = 0; i < pres.slidArray.length; i++) {
        if (pres.slidArray[i].id === slide.id) {
            // update slide
            pres.slidArray[i] = slide;
            // force l'actualisation
            return Object.assign({}, pres);
        }
    }
    // add slide
    pres.slidArray.push(slide);
    return Object.assign({}, pres);
}

export default updateModelReducer;