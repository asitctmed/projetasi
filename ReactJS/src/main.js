import React, { Component } from 'react';
import './lib/bootstrap-3.3.7-dist/css/bootstrap.min.css'
import './index.css';
import '../src/components/common/content/containers/visual.css'; //bug si importé dans visual.js
import BrowseContentPanel from './components/browseContentPanel/browseContentPanel';
// import * as contentMap from './sources/contentMap.json';
// import * as pres from './sources/pres.json';
import BrowsePresentationPanel from './components/browsePresentationPanel/containers/browsePresentationPanel';
import EditSlidPanel from './components/editSlidPanel/containers/editSlidPanel';
import Comm from './services/comm';

// store
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import globalReducer from './reducers/index';
import { updateContentMap, updatePresentation } from './actions';
const store = createStore(globalReducer);




export default class Main extends Component {
    constructor(props) {
        super(props);
        this.comm = new Comm();
        this.getData();
        // store.dispatch(updatePresentation(pres.default));
        // store.dispatch(updateContentMap(contentMap.contents));
        // this.subscribe();   
    }

    subscribe = () => {
        store.subscribe(() => {
            // this.setState({
            //         presentation: store.getState().updateModelReducer.pres
            //     }
            // );
            // this.setState({
            //         contentMap: store.getState().updateModelReducer.contentMap
            //     }
            // );
            if (store.getState().commandReducer.cmdPres === 'SAVE_CMD') {
                console.log("on y est");
                this.comm.savPres(store.getState().updateModelReducer.pres, this.callbackErr);
            }
        });
    }

    getData = () => {
        const comm = new Comm({});
        comm.loadPres(1,
            (loadPres) => { store.dispatch(updatePresentation(loadPres)); },
            this.callbackErr
        );
        comm.loadContent(
            (loadContentMap) => { store.dispatch(updateContentMap(loadContentMap)); },
            this.callbackErr
        );
    }

    callbackErr = (err) => {
        console.log(err);
    }

    render() {
        return (
            <Provider store={store} >
                <div className='container-fluid height-100'>
                    <div className="row height-100">
                        <div className='col-md-3 col-lg-3'>
                            <BrowsePresentationPanel />
                        </div>
                        <div className='col-md-6 col-lg-6 vertical-scroll '>
                            <EditSlidPanel />
                        </div>
                        <div className='col-md-3 col-lg-3 vertical-scroll'>
                            <BrowseContentPanel />
                        </div>
                    </div>
                </div>
            </Provider>
        );
    }
}