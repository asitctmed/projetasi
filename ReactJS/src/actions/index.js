export const setSelectedSlid = (slid_obj) => {
    return {
        type: 'UPDATE_SELECTED_SLID',
        obj: slid_obj
    };
}

export const updateContentMap = (contentMap) => {
    return {
        type: 'UPDATE_CONTENT_MAP',
        obj: contentMap
    };
}

export const updateSlid = (slid) => {
    return {
        type: 'UPDATE_PRESENTATION_SLIDS',
        obj: slid
    };
}

export const deleteSlid = (slidId) => {
    return {
        type: 'DELETE_SLID',
        obj: slidId
    };
}

export const updatePresentation = (pres) => {
    return {
        type: 'UPDATE_PRESENTATION',
        obj: pres
    };
}

export const updateDraggedElt = (content_id) => {
    return {
        type: "UPDATE_DRAGGED_ELT",
        obj: content_id
    }
}

export const addContent = (newContent) => {
    return {
        type: "ADD_CONTENT",
        obj: newContent
    }
}

