import React, { Component } from 'react';
import { connect } from 'react-redux';
import Slid from '../../common/slid/containers/slid';
import { updateSlid, setSelectedSlid } from '../../../actions';

// • selected_slid : slid à afficher
// • contentMap : Dictionnaire des contenus disponible issu du fichier json
// contentMap.json

//• Affiche le slid sélectionné en mode édition (FULL_MNG)
class EditSlidPanel extends Component {

    render() {
        let slidDisplay = "";
        if (this.props.selected_slid && this.props.selected_slid.id !== undefined) {
            if (this.props.selected_slid != null) {
                slidDisplay = (<Slid s={this.props.selected_slid}
                    displayMode={"full_mng"} handleChange={this.updateCurrentSlid} onlyContent={true} />)
            }
        }
        return (
            <div onDrop={this.onDrop} onDragOver={this.allowDrop}>
                {slidDisplay}
            </div>
        );
    }

    updateCurrentSlid = field => ({ target: { value } }) => {
        let title = this.props.selected_slid.title;
        let txt = this.props.selected_slid.txt;
        if (field === "title")
            title = value;
        else
            txt = value;

        const tmpSlid = {
            id: this.props.selected_slid.id,
            title: title,
            txt: txt,
            content_id: this.props.selected_slid.content_id
        }
        // update selected slide and slides presentation
        this.props.dispatch(setSelectedSlid(tmpSlid));
        this.props.dispatch(updateSlid(tmpSlid));
    }

    onDrop = (event) => {
        event.preventDefault()
        const tmpSlid = {
            id: this.props.selected_slid.id,
            title: this.props.selected_slid.title,
            txt: this.props.selected_slid.txt,
            content_id: this.props.content_id,
        }
        // update selected slide and slides presentation
        this.props.dispatch(setSelectedSlid(tmpSlid));
        this.props.dispatch(updateSlid(tmpSlid));
    }

    allowDrop = (event) => {
        event.preventDefault();
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        selected_slid: state.selectedReducer.slid,
        contentMap: state.updateModelReducer.contentMap,
        content_id: state.selectedReducer.content_id,
    }
};

export default connect(mapStateToProps)(EditSlidPanel);