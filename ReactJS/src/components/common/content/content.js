import React, { Component } from 'react';
import Visual from './containers/visual';
import Details from './containers/details';
import { updateDraggedElt } from '../../../actions';
import { connect } from 'react-redux';

// id
// src
// type
// title
// only content
class Content extends Component {

    onDragStart = (event) => {
        this.props.dispatch(updateDraggedElt(this.props.c.id));
      }

    render() {
        return (
            <div className="panel panel-default" draggable="true" onDragStart={this.onDragStart}>
                <Visual src={this.props.c.src} type={this.props.c.type} />
                <Details c={this.props.c} onlyContent={this.props.onlyContent}/>
            </div>
        );
    }
}


export default connect()(Content);