import React, { Component } from 'react';

class Details extends Component {

    render() {
        let details="";
        if (this.props.onlyContent === false)
        {
            details = (<h5>ID :{this.props.c.id}, Title : {this.props.c.title}, 
                Type : {this.props.c.type}</h5>); 
        }
        return(
            <div>{details}</div>
        );
    }
}

export default Details;