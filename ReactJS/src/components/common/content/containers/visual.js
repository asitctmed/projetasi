import React, { Component } from 'react';
//import './visual.css';

class Visual extends Component {

    render() {
        let render_visual;
        switch (this.props.type) {
            case "img_url":
                render_visual = (
                    <img className='imgCard' src={this.props.src} alt="url"/>
                );
                break;
            case "video":
                // replace pour youtube
                const url = this.props.src.replace("watch?v=", "embed/");
                render_visual = (
                    <object width="100%" height="100%" data={url} alt="video"/>
                );
                break;
            case "img":
                render_visual = (
                    // à changer quand mise en commmun avec ED
                    <img className='imgCard' src={this.props.src} alt="base64"/>
                );
                break;
            case "web":
                render_visual = (
                    <iframe title="Inline Frame Example" width="100%" height="100%" src={this.props.src} alt="webSite"/>
                );
                break;
            default:
                render_visual = (<h5>nothing</h5>)
                break;

        }


        return (
            <div className="thumbnail">
                {render_visual}
            </div>
        );
    }
}

export default Visual;