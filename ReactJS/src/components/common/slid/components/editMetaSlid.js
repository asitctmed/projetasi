import React from 'react';
import './editMetaSlid.css'

class EditMetaSlid extends React.Component {

    render() {
        return (
            <div className="form-group">
                <label htmlFor="currentSlideTitle">Title</label>
                <input value={this.props.title} name="title" type="text" className="form-control" id="currentSlideTitle"
                    onChange={this.props.handleChangeTitle} />
                <label htmlFor="currentSlideText">Text</label>
                <textarea value={this.props.txt} name="txt" rows="5" type="text" className="form-control" id="currentSlideText"
                    onChange={this.props.handleChangeTxt}>
                </textarea>
            </div>
        );
    }
}
export default (EditMetaSlid);
