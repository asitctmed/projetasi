import React, { Component } from 'react';
import Content from '../../content/content';
import EditMetaSlid from '../components/editMetaSlid';
import { connect } from 'react-redux';
import { setSelectedSlid } from '../../../../actions'
import './slid.css'

// • id : id du slid
// • title : titre du slid
// • txt : texte de description du slid
// • content_id : ID du Contenu du slid
// • contentMap 
// • displayMode : String : ‘SHORT’ ou ‘FULL_MNG’
//• SHORT: Affiche les propriétés du slid et le composant Content associé
// • FULL_MNG : Affiche un formulaire d’édition des propriétés du Slid et le composant Content associé
class Slid extends Component {

    updateSelectedSlid = () => {
        const tmpSlid = {
            id: this.props.s.id,
            title: this.props.s.title,
            txt: this.props.s.txt,
            content_id: this.props.s.content_id
        };
        // dispatch an action and put the tmpSlid in the store
        // change the state of the store
        this.props.dispatch(setSelectedSlid(tmpSlid));
    }

    render() {
        let contentDisplay = "";
        let editMetaSlidDisplay = "";
        if (this.props.contentMap.length !== undefined) {
            let cont = this.props.contentMap.find(row => row.id === this.props.s.content_id);
            if (cont != null)
                contentDisplay = (<Content c={cont} onlyContent={this.props.onlyContent} />);

            if (this.props.displayMode === "full_mng") {
                editMetaSlidDisplay = (<EditMetaSlid title={this.props.s.title} txt={this.props.s.txt}
                    handleChangeTitle={this.props.handleChange('title')}
                    handleChangeTxt={this.props.handleChange('txt')} />);
            }
        }

        return (
            <div className="panel panel-default" onClick={() => this.updateSelectedSlid()}>
                <h4 id="title">{this.props.s.title}</h4>
                <h5>{this.props.s.text}</h5>
                {contentDisplay}
                {editMetaSlidDisplay}
            </div>
        );
    }

}

const mapStateToProps = (state, ownProps) => {
    return {
        contentMap: state.updateModelReducer.contentMap,
    }
};

export default connect(mapStateToProps)(Slid);
