import React, { Component } from 'react';
import Slid from '../../../common/slid/containers/slid';
import { connect } from 'react-redux';
import './slidList.css';

class SlidList extends Component {

    getAllSlidRender = () => {
        let array_render = [];
        if (this.props.pres.slidArray !== undefined){
            for (var i = 0; i < this.props.pres.slidArray.length; i++) {
                array_render.push(
                    <Slid
                        key={i}
                        s={this.props.pres.slidArray[i]}
                    />
                );
            }
        }
        return array_render;
    }

    render() {
        let diplayList = this.getAllSlidRender();
        return (
            <div className="vertical-scroll" id="divSlidList">
                {diplayList}
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        pres: state.updateModelReducer.pres
    }
};

export default connect(mapStateToProps)(SlidList);
