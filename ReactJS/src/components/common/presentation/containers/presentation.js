import React, { Component } from 'react';
import SlidList from '../components/slidList';
import { connect } from 'react-redux';
import CommandButtons from '../../../browsePresentationPanel/containers/commandButtons';


// • id : id de la présentation
// • title : titre du slid
// • description : texte décrivant la présentation
// • slidArray :tableau de slids
// • contentMap : Dictionnaire des contenus disponible issu du fichier json contentMap.json

// • Affiche un formulaire d’édition des propriétés de la présentation
// • Affiche la liste des slids de la présentation (displayMode : SHORT)
class Presentation extends Component {

    render() {
        return (
            <div>
                <div className="form-group">
                    <label htmlFor="presTitle">Title</label> 
                    <input type="text" className="form-control" id="presTitle" 
                        value={this.props.pres.title} readOnly/>
                    <label htmlFor="presDesc">Description</label>
                    <textarea rows="3" type="text" className="form-control" id="presDesc"
                        value={this.props.pres.description} readOnly></textarea>
                </div>
                <CommandButtons/>
                <SlidList/>
            </div>
        );
    }

}

const mapStateToProps = (state, ownProps) => {
    return {
        pres: state.updateModelReducer.pres
    }
};

export default connect(mapStateToProps)(Presentation);
