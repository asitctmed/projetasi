import React, { Component } from 'react'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import MuiThemProvider from 'material-ui/styles/MuiThemeProvider'
import { connect } from 'react-redux'
import { addContent } from '../../../actions';

class AddContentPanel extends Component {

    constructor(props) {
        super(props);

        this.state = {
            type: 'video',
            open: false,
            title: '',
            src: '',
        }
    }


    handleOpen = () => {
        this.setState({ open: true })
    }

    handleClose = () => {
        this.setState({ open: false })
    }

    handleChangeType = ({ target: { value } }) => this.setState({ type: value })

    handleChangeTitle = ({ target: { value } }) => this.setState({ title: value })

    handleChangeUrl = ({ target: { value } }) => this.setState({ src: value })

    addContent = () => {
        const content = {
            title: this.state.title,
            type: this.state.type,
            src: this.state.src
        };    
        this.props.dispatch(addContent(content));
        this.setState({ open: false , title: "", src: "", type: "video"})
    }

    render() {
        const actions = [
            <FlatButton label="Cancel" primary onClick={this.handleClose} />,
            <FlatButton label="Submit" primary onClick={this.addContent} />,
        ]

        return (
            <MuiThemProvider>
                <div>
                    <RaisedButton label="Add Content" onClick={this.handleOpen} />
                    <Dialog
                        title="Add a new content"
                        actions={actions}
                        modal={false}
                        open={this.state.open}
                        onRequestClose={this.handleClose}
                    >
                        <div className="form-group">
                            <label htmlFor="new-content-title">Title</label>
                            <input className="form-control"
                                type="text"
                                id="new-content-title"
                                onChange={this.handleChangeTitle}
                                value={this.state.title}
                            />
                        </div>

                        <div className="form-group">
                            <label htmlFor="new-content-type">Content type</label>
                            <select className="form-control" 
                                id="new-content-type" 
                                value={this.state.type}
                                onChange={this.handleChangeType}
                            >
                                <option value="video">Video</option>
                                <option value="img_url">Img URL</option>
                                <option value="img">Img</option>
                                <option value="web">Web</option>
                            </select>
                        </div>

                        <div className="form-group">
                            <label htmlFor="new-content-url">URL</label>
                            <input className="form-control"
                                type="text"
                                id="new-content-url"
                                onChange={this.handleChangeUrl}
                                value={this.state.src}
                            />
                        </div>
                    </Dialog>
                </div>
            </MuiThemProvider>
        )
    }
}

export default connect()(AddContentPanel)
