import React, { Component } from 'react';
import './commandButtons.css';
import { connect } from 'react-redux';
import { updateSlid, deleteSlid, setSelectedSlid } from '../../../actions';
import Tools from '../../../services/tools';
import { sendNavCmd } from '../../../actions/commandAction';
class CommandButton extends Component {


    handleAdd = () => {
        const tmpSlide = {
            id: Tools.generateUUID(),
            title: "title",
            txt: "txt",
            content_id: ""
        }
        this.props.dispatch(updateSlid(tmpSlide));
    }

    handleRemove = () => {
        if (this.props.selected_slid !== null && this.props.selected_slid.id !== undefined) {
            this.props.dispatch(deleteSlid(this.props.selected_slid.id))
            this.props.dispatch(setSelectedSlid(null));
        }
    }

    handleSave = () => {
        this.props.dispatch(sendNavCmd("SAVE_CMD"));
    }

    render() {
        return (
            <div className="div-center form-group">
                <button className="btn btn-success button-margin-right" onClick={this.handleAdd}>Add</button>
                <button className="btn btn-danger button-margin-right" onClick={this.handleRemove}>Remove</button>
                <button className="btn btn-primary" onClick={this.handleSave}>Save</button>
            </div>
        );
    };
}

const mapStateToProps = (state, ownProps) => {
    return {
        selected_slid: state.selectedReducer.slid
    }
};

export default connect(mapStateToProps)(CommandButton);