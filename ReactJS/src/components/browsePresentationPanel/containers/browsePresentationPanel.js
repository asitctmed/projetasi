import React, { Component } from 'react';
import Presentation from '../../common/presentation/containers/presentation';

// • selected_slid : slid à afficher
// • contentMap : Dictionnaire des contenus disponible issu du fichier json
// contentMap.json

//• Affiche le slid sélectionné en mode édition (FULL_MNG)
class BrowsePresentationPanel extends Component {

    render() {
        return (
            <div>
                <Presentation/>
            </div>
        );
    }
}


export default BrowsePresentationPanel;