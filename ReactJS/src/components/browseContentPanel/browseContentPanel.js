import React, { Component } from 'react';
import Content from '../common/content/content'
import { connect } from 'react-redux';
import AddContentPanel from '../addContentPanel/containers/addContentPanel';
import './browseContentPanel.css'

class BrowseContentPanel extends Component {

    getAllContentsRender = () => {
        let array_render = [];
        for (var i = 0; i < this.props.contentMap.length; i++) {
            let content = this.props.contentMap[i];
            content.onlyContent = false;
            array_render.push(
                <Content
                    key={i}
                    c={content}
                    onlyContent={false}
                />
            );
        }
        return array_render;
    }

    render() {
        const display_list = this.getAllContentsRender();
        return (
            <div className="ajustementsForButtonAddContent">
                {display_list}
                <AddContentPanel/>
            </div>
        );

    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        contentMap: state.updateModelReducer.contentMap
    }
};

export default connect(mapStateToProps)(BrowseContentPanel);
