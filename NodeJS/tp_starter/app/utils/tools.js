let Tools={}

Tools.generateUUID= function(){
       var d = new Date().getTime();
       var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c)      {
            var r = (d + Math.random()*16)%16 | 0;
            d = Math.floor(d/16);
                return (c=='x' ? r : (r&0x3|0x8)).toString(16);
        });
        return uuid;
    };

Tools.getNextSlidId= function(array,slidId){
    if (array != null) {
        for(var i=0;i<array.length;i++){
            if(slidId==array[i].id && (i+1 < array.length)) {
                slidId = array[i+1].id;
                break;
            }
        }
    }
    
    return slidId;
}

Tools.getPrevSlidId=function(array,slidId){
    if (array !== null) {
        for(var i=0;i<array.length;i++){
            if((i != 0) && (slidId==array[i].id)){
                slidId = array[i-1].id;
                break;
            }
        }
    }

    return slidId;
}

Tools.getSlideFromId=function(array,slidId){
    let slide = null;

    if (array !== null) {
        for(var i=0;i<array.length;i++){
            if(slidId==array[i].id){
                slide = array[i];
                break;
            }
        }
    }

    return slide;
}

Tools.getFirstSlideId=function(array){
    let slideId = -1;
    if (array !== null) {
        slideId = array[0].id;
    }

    return slideId;
}

module.exports = Tools;
