'use strict';

let express = require("express");
let router = express.Router();
let bodyParser = require('body-parser');
let multer = require("multer");

let contentController = require('../controllers/content.controller.js');
let multerMiddleware = multer({"dest":"/tmp/"});
router.use(bodyParser.json());

module.exports = router;

router.route('/contents').get(contentController.list);
router.route('/resources_list').get(contentController.contentMap);
router.post("/contents", multerMiddleware.single("file"), contentController.create);

router.route('/contents/:contentId')
    .get(contentController.read)
    .delete(contentController.delete);
router.put('/contents/:contentId', multerMiddleware.single("file"), contentController.update);

router.param('contentId', function(req, res, next, id) {
    req.userId = id;
    next();
})
