'use strict'
const express = require("express");
const fs = require("fs");

const CONFIG = require("../../config.json");
const path = require("path");

const router = express.Router();
const bodyParser = require('body-parser');

router.use(bodyParser.json());
module.exports = router;

function convertContentMapsToId(fileJson){
    let slidArray = fileJson.slidArray;
    let slide;

    for(var i=0;i<slidArray.length;i++){
        slide = slidArray[i];
        slide.content_id = slide.contentMap['1'];
        delete slide.contentMap;
        slidArray[i] = slide;
    }
    fileJson.slidArray = slidArray;

    return fileJson;
}

function convertContentIdsToMap(fileJson){
    let slidArray = fileJson.slidArray;
    let slide;

    for(var i=0;i<slidArray.length;i++){
        slide = slidArray[i];
        slide['contentMap'] = {};
        slide.contentMap['1'] = slide.content_id;
        delete slide.content_id;
        slidArray[i] = slide;
    }
    fileJson.slidArray = slidArray;

    return fileJson;
}

router.route('/loadPres').get((req, res) => {
    let CONFIG = JSON.parse(process.env.CONFIG);
    let presDir = CONFIG["presentationDirectory"];
    let resMap = {};
    let jsonFiles = [];

    fs.readdir(presDir, (err, content) => {
        //Récupère tous les fichiers json du dossier
        content.forEach(file => {
            if (path.extname(file) === '.json'){
                jsonFiles.push(file);
            }
        });
        //Parcours tous les fichiers et stocke leur données dans une map
        jsonFiles.forEach(file => {
            let pathFile = path.join(presDir, file);
            fs.readFile(pathFile, (err, data) => {
                let fileJson = JSON.parse(data);
                
                fileJson = convertContentMapsToId(fileJson);
                resMap[fileJson.id] = fileJson;
                if (Object.keys(resMap).length === jsonFiles.length)
                {   
                    return res.json(resMap);    
                }
            })
        })
    });
});

router.route('/savePres').post((req, res) => {
    let CONFIG = JSON.parse(process.env.CONFIG);
    let presDir = CONFIG["presentationDirectory"];

    //Obtient l'objet json du body de la requête
    let jsonBody = req.body;
    let fileId = jsonBody['id'];
    //Construit le nom du fichier suivant l'id du body
    let pathFile = presDir + "/" + fileId + ".pres.json";

    jsonBody = convertContentIdsToMap(jsonBody);

    //Crée ou modifie le fichier s'il existe déjà
    fs.writeFile(pathFile, JSON.stringify(jsonBody), () => {
        return res.json(jsonBody);
    });
});
