'use strict';

const fs = require("fs");
const CONFIG_PATH = require("../../config.json");
const CONFIG = JSON.parse(process.env.CONFIG);
const path = require("path");
//Clé utilisée pour référencer l'attribut privé
const DATA = Symbol("data");


module.exports = class ContentModel
{
    constructor(content){
        if (content == undefined){
            content = {};
        }
        
        this.type = content['type'];
        this.id = content['id'];
        this.title = content['title'];
        this.src = content['src'];
        this.fileName = content['fileName'];
        this[DATA] = null;
    }

    setData(value){
       this[DATA] = value;
    }

    getData(){
        return this[DATA];
    }

    static create(content, callback) {
        if (content == null || !(content instanceof ContentModel) || content.id == null) {
            return callback(new Error("The content parameter is not correct"));
        }
        let contentDir = CONFIG["contentDirectory"];
        let error;
        
        //Si c'est une image, un fichier contenant les data est créé
        if (content.type === "img"){
            let pathFile = contentDir + "/" + content.fileName;
            fs.writeFile(pathFile, content.getData(), 'binary', (err) => {
                error = err;
            });
        }

        let pathFile = contentDir + "/" + content.id + ".meta.json";
        fs.writeFile(pathFile, JSON.stringify(content), (err) => {
            error = err;
        });

        return callback(error, content);
    }

    static list(callback) {
        let contentDir = CONFIG["contentDirectory"];

        let resMap = {};
        let jsonFiles = [];
        let jsonFilesId = [];

        fs.readdir(contentDir, (readDirError, content) => {
            if (readDirError) return callback(new Error("json files couldn't be read"), null);
            //Récupère tous les fichiers json du dossier
            content.forEach(file => {
                if (path.extname(file) === '.json') {
                    jsonFilesId.push(path.basename(file, '.meta.json'));
                    jsonFiles.push(file);
                }
            });
            jsonFilesId.forEach(fileId => {
                ContentModel.read(fileId, (err, data) => {
                    resMap[fileId] = data;

                    if (Object.keys(resMap).length === jsonFiles.length) {   
                        return callback(err, resMap);    
                    }
                });
            })
        });
    }

    static read(id, callback) {
        if (id === null) return callback(new Error('Id is null'), null);

        let pathFile = CONFIG["contentDirectory"] + "/" + id + ".meta.json";

        //Vérifie qu'on a accès au fichier et qu'il existe
        fs.access(pathFile, (accessError) => {
            if (accessError) return callback(accessError, null);

            fs.readFile(pathFile, (readError, data) => {
                callback(readError, new ContentModel(JSON.parse(data)));    
            });
        })
    }

    static update(content, callback) {
        if (content == null || !(content instanceof ContentModel) || content.id == null) {
            return callback(new Error("The content parameter is not correct"));
        }

        let pathFile = CONFIG["contentDirectory"] + "/" + content.id + ".meta.json";
        let error;
        
        //Vérifie qu'on a accès au fichier et qu'il existe
        fs.access(pathFile, (accessError) => {
            if (accessError) return callback(accessError, null);

            //Met à jour les méta données
            fs.writeFile(pathFile, JSON.stringify(content), (err) => {
                error = err;
            });

            //Si c'est une image, le fichier contenant les data est mis à jour
            if (content.type === "img" && content.getData() !== null && content.getData().length > 0){
                pathFile = CONFIG["contentDirectory"] + "/" + content.fileName;
                fs.writeFile(pathFile, content.getData(), (err) => {
                    error = err;
                });
            }
            
            return callback(error, content);
        })
    }

    static delete(id, callback) {
        if (id === null) return callback(new Error('Id is null'), null);

        let pathFile = CONFIG["contentDirectory"] + "/" + id + ".meta.json";
        let content = null;

        fs.access(pathFile, (accessError) => {
            if (accessError) return callback(accessError, null);

            //Récupère l'objet lié à l'id
            fs.readFile(pathFile, (readError, data) => {
                if (readError) return callback(readError, null);
                content = new ContentModel(JSON.parse(data));
                //Supprime le fichier métadata
                fs.unlink(pathFile, (unlinkError) => {
                    if (unlinkError) return callback(unlinkError, null);    
                });

                //Supprime le fichier data si le contenu est de type image
                if (content.type === "img"){
                    pathFile = CONFIG["contentDirectory"] + "/" + content.fileName;
                    fs.unlink(pathFile, (unlinkError) => {
                        if (unlinkError) return callback(unlinkError);    
                    });    
                }

                return callback(null, content);
            });
        })
    }
}
