'use strict';

const CONFIG = JSON.parse(process.env.CONFIG);
const ContentModel = require('../models/content.model.js');
const nodeUuid = require('node-uuid');
const fs = require("fs");

module.exports = class ContentController
{
    static list(req, res) {
        ContentModel.list((err, data) => {
            if (err) return res.status(500).send(err.message);
            return res.json(data);
        });
    };

    static contentMap(req, res) {
        let jsonData = {};
        jsonData.contents = [];
        ContentModel.list((err, data) => {
            if (err) return res.status(500).send(err.message);
            for(let elementId in data){
                jsonData.contents.push(data[elementId]);
            }
            
            return res.json(jsonData);
        });
    };

    static create(req, res) {
        let contentInstance = new ContentModel(req.body);
        contentInstance.id = nodeUuid.v4();
        if (req.file && contentInstance.type === "img") {
            let extension = "." + req.file.originalname.split('.').pop();
            
            if (contentInstance.fileName == null) {
                contentInstance.fileName = contentInstance.id + extension;
            }

            fs.readFile(req.file.path, function(err, data) {
                contentInstance.setData(data);
                ContentModel.create(contentInstance , (err, data) => {
                    if (err) return res.status(500).send(err.message);
                    return res.json(data);
                });
            });
        } else {
            ContentModel.create(contentInstance , (err, data) => {
                if (err) return res.status(500).send(err.message);
                return res.json(data);
            });
        }
    }

    static read (req, res) {
        ContentModel.read(req.userId, (err, contentInstance) => {
            if (err) return res.status(500).send(err.message);

            //Renvoie les métas données si l'url contient "?json=true"
            if (req.query.json === "true") return res.json(contentInstance);
            
            //Affiche le fichier présent sur le server si c'est une image
            if (contentInstance.type === 'img') {
                return res.sendFile(CONFIG['contentDirectory'] + "/" + contentInstance.fileName);
            }

            //Envoie une redirection vers l'url externe du contenu demandé si ce n'est pas une image
            res.writeHead(301, {Location: contentInstance.src });
            return res.end();
        });
    }

    static update(req, res) {
        let contentInstance = new ContentModel(req.body);
        //Si aucun id n'a été définit dans le body, on utilise l'id renseigné dans l'URL
        if (typeof contentInstance.id === "undefined") contentInstance.id = req.userId;
        
        if (req.file && contentInstance.type === "img") {
            fs.readFile(req.file.path, function(err, data) {
                contentInstance.setData(data);
                ContentModel.update(contentInstance , (err, data) => {
                    if (err) return res.status(500).send(err.message);
                    return res.json(data);
                });
            });
        } else {
            ContentModel.update(contentInstance, (err, updatedContent) => {
                if (err) return res.status(500).send(err.message);
                return res.json(updatedContent);
            });
        }
    }

    static delete(req, res) {
        ContentModel.delete(req.userId, (err, deletedContent) => {
            if (err) return res.status(500).send(err.message);
            return res.json(deletedContent);
        });
    }
};
