'use strict';

const SocketIO = require('socket.io');
const fs = require("fs");
const CONFIG = require("../../config.json");
const path = require("path");
const tools = require("../utils/tools.js");

const ContentModel = require("../models/content.model.js");

module.exports = class IOController
{
    static listen(server){
        let io = new SocketIO(server);
        let watchersMap = new Map();

        let presentation = null;
        let slidArray = null;
        let currentSlideId = "";
        let currentSlide = null;
        
        io.on('connection', function(socket){
            console.log("User " + socket.id + " connected");

            //Envoie une confirmation de connexion au client qui a souhaité se connecté
            socket.emit('connection', 'Vous êtes connecté !');

            //Ecoute les events data_comm, ajoute le client expéditeur dans la map 
            socket.on('data_comm', function (params) {
                watchersMap.set(params.id, socket);
                console.log("User " + params.id + " added to the list of watchers");
            });
            
            //Ecoute les events slidEvent, qui contrôle le déroulement d'une présentation
            socket.on('slidEvent', function (jsonData) {
                switch(jsonData.CMD) {
                    case "START":
                        presentation = IOController.loadPresentation(jsonData.PRES_ID);
                        slidArray = presentation.slidArray;
                        console.log("Présentation choisie: " + presentation.title);
                        break;
                    case "PAUSE":
                        console.log("Pause");
                        break;
                    case "END":
                        currentSlideId = "";
                        currentSlide = null;

                        console.log("Fin");
                        //broadcast emit avec fermeture du diapo
                        break;
                    case "BEGIN":
                        currentSlideId = tools.getFirstSlideId(slidArray);
                        currentSlide = tools.getSlideFromId(slidArray, currentSlideId);
                        if (currentSlide != null) {
                            IOController.sendSlide(watchersMap, currentSlide);
                        }

                        console.log("Début: slideId = " + currentSlideId);
                        //broadcast emit avec current slide + content
                        break;
                    case "PREV":
                        currentSlideId = tools.getPrevSlidId(slidArray, currentSlideId);
                        currentSlide = tools.getSlideFromId(slidArray, currentSlideId);
                        IOController.sendSlide(watchersMap, currentSlide);

                        console.log("Précédent: slideId = " + currentSlideId);
                        //broadcast emit avec current slide + content
                        break;
                    case "NEXT":
                        currentSlideId = tools.getNextSlidId(slidArray, currentSlideId);
                        currentSlide = tools.getSlideFromId(slidArray, currentSlideId);
                        IOController.sendSlide(watchersMap, currentSlide);

                        console.log("Suivant: slideId = " + currentSlideId);
                        //broadcast emit avec current slide + content
                        break;
                };
            });

        });
    }

    //Prépare l'envoie de la slide courante aux watchers
    static sendSlide(watchersMap, currentSlide) {
        if (currentSlide != null) {
            //Charge le contenu si un contenuId est déclaré dans la slide courante
            if (("contentMap" in currentSlide) && ('1' in currentSlide.contentMap)) {
                let slideContentId = currentSlide.contentMap['1'];

                ContentModel.read(slideContentId, (err, content) => {
                    if (err) console.log('Error reading slide content : ' + err.message);
                    currentSlide['content'] = content;
                    IOController.emitToWatchers(watchersMap, currentSlide);
                });
            } else {
                IOController.emitToWatchers(watchersMap, currentSlide);
            }
        }
    }

    //Émet la slide courrante aux watchers
    static emitToWatchers(watchersMap, currentSlide) {
        for (var socket of watchersMap.values()) {
            socket.emit('currentSlidEvent', JSON.stringify(currentSlide));
        };
    }

    //Charge une présentation selon son id
    static loadPresentation(presId) {
        let pathFile = path.join(CONFIG['presentationDirectory'], presId + '.pres.json');
        
        return JSON.parse(fs.readFileSync(pathFile));
    }
}
