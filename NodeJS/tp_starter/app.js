'use strict'
var express = require("express");
var http = require("http");
var CONFIG = require("./config.json");
var path = require("path");

var app = express();
var router = express.Router();
process.env.CONFIG = JSON.stringify(CONFIG);

// init server
var server = http.createServer(app);

//Server Websocket
var IOController = require("./app/controllers/io.controller.js");
IOController.listen(server);

server.listen(CONFIG.port);

// routes
app.use(require("./app/routes/default.route.js"));
app.use(require("./app/routes/presentation.route.js"));
app.use(require("./app/routes/content.route.js"));

// static routes
app.use("/admin", express.static(path.join(__dirname, "public/admin")));
app.use("/watch", express.static(path.join(__dirname, "public/watch")));