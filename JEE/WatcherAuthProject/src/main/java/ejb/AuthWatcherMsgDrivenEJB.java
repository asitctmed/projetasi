package ejb;

import java.util.Date;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;


import model.DataContainer;
import model.UserDAO;
import models.*;

@MessageDriven(
        activationConfig = {
                @ActivationConfigProperty(
                        propertyName = "destinationType",
                        propertyValue = "javax.jms.Topic"),
                @ActivationConfigProperty(
                        propertyName = "destination",
                        propertyValue = "java:/jms/watcherAuthJMS")
        })

public class AuthWatcherMsgDrivenEJB implements MessageListener {
    //private DataContainer dataContainer;

    @EJB
    MessageSenderQueueLocal sender;

    @Inject
    UserDAO userDao;

    /*public AuthWatcherMsgDrivenEJB() {
        dataContainer = new DataContainer();
    }*/

    public void onMessage(Message message) {
        try {
            if (message instanceof TextMessage) {
                System.out.println("Topic: I received a TextMessage at "
                        + new Date());
                TextMessage msg = (TextMessage) message;
                System.out.println("Message is : " + msg.getText());
            } else if (message instanceof ObjectMessage) {
                System.out.println("Topic: I received an ObjectMessage at "
                        + new Date());
                ObjectMessage msg = (ObjectMessage) message;

                if (msg.getObject() instanceof UserDTO) {
                    UserDTO user = (UserDTO) msg.getObject();

                    System.out.println("User Details: ");
                    System.out.println("login:" + user.getLogin());
                    System.out.println("pwd:" + user.getPwd());

                    //Create new user response
                    UserResponse userResponse = new UserResponse();
                    userResponse.setLogin(user.getLogin());
                    //Role currentTestRole = dataContainer.checkUser(user);
                    Role currentTestRole = userDao.checkUser(user);

                    if (Role.None == currentTestRole) {
                        userResponse.setValidAuth(false);
                    } else {
                        userResponse.setValidAuth(true);
                    }

                    userResponse.setRole(currentTestRole);

                    System.out.println(userResponse.getLogin());
                    System.out.println(userResponse.getRole());
                    sender.sendMessage(userResponse);


                }
            } else {
                System.out.println("Not valid message for this Queue MDB");
            }
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

}