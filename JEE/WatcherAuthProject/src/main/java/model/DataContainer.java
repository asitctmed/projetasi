package model;

import models.Role;
import models.UserDTO;
import models.UserModel;
import models.UserResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to simulate a BDD
 */

public class DataContainer {

    public Role checkUser(UserDTO user) {

        List<UserModel> liUser = new ArrayList<>();

        UserModel us1 = new UserModel();
        us1.setLogin("bg69");
        us1.setLastname("durousset");
        us1.setSurName("etienne");
        us1.setRole(Role.User);
        us1.setPwd("etidur");

        UserModel us2 = new UserModel();
        us2.setLogin("jule69");
        us2.setLastname("cordier");
        us2.setSurName("thomas");
        us2.setRole(Role.Admin);
        us2.setPwd("krokro");

        liUser.add(us1);
        liUser.add(us2);

        for (UserModel us : liUser) {
            if(us.getLogin().equals(user.getLogin()) && us.getPwd().equals(user.getPwd())){
                return us.getRole();
            }
        }
        return Role.None;
    }
}
