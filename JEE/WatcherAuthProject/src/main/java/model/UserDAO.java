package model;

import models.Role;
import models.UserDTO;
import models.UserModel;

import javax.ejb.Local;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Class to allow acces with BDD
 */
@Singleton
public class UserDAO {

    @PersistenceContext
    EntityManager em;

    public UserModel getUserByLogin(String login){

        UserModel us = new UserModel();
        us.setRole(Role.None);
        us.setLogin(login);
        //us.setPwd(pwd);

        try {
//            udb = (UserModel) em.createQuery("SELECT u FROM UserModel u WHERE u.login = :login")
//                    .setParameter("login",login)
//                    .getSingleResult();
//
//            if(udb.getPwd().equals(us.getPwd())){
//                return udb;
//            }

            UserModel udb = (UserModel)em.createNamedQuery("Users.byLogin").setParameter("login",login).getSingleResult();

            return udb;

        } catch (Exception e){
            System.out.println("UserNotFound Exception ! ");
        }

        return us;
    }


    public Role checkUser(UserDTO user) {

        //UserModel userModel = getUserByLogin(user.getLogin(),user.getPwd());
        UserModel userModelFromDB = getUserByLogin(user.getLogin());

//        if(userModel.getLogin().equals(user.getLogin()) && userModel.getPwd().equals(user.getPwd())){
//            return userModel.getRole();
//        }

        if(userModelFromDB!=null){
            if(user.getPwd().equals(userModelFromDB.getPwd())){
                return userModelFromDB.getRole();
            }
        }

        return Role.None;
    }
}
