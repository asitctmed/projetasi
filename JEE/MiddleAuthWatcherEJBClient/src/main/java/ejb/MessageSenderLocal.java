package ejb;

import models.UserDTO;
import models.UserModel;
import models.UserResponse;

import javax.ejb.Local;

@Local

public interface MessageSenderLocal {

    void sendMessage(String message);

    void sendMessage(UserDTO user);
}
