package ejb;

import models.UserModel;
import models.UserResponse;

import javax.ejb.Local;

@Local

public interface MessageReceiverSyncLocal {


    UserResponse receiveMessage();
}
