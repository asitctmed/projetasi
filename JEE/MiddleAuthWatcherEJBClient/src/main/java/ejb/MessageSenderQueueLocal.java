package ejb;

import models.UserModel;
import models.UserResponse;

import javax.ejb.Local;

@Local

public interface MessageSenderQueueLocal {

    public void sendMessage(String message);
    public void sendMessage(UserResponse user);
}
