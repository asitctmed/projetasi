package models;


import java.util.Objects;

/**
 * Possibilities --> Role for an user
 *//*
public enum Role {

    ADMIN("ADMIN"), USER("USER"), NONE(null);

    private final String role;

    Role(final String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return this.role;
    }

    public static Role getEnumString(String s) {
        for (Role r : Role.values()) {
            if (Objects.equals(r.role, s)) {
                return r;
            }
        }
        return null;
    }

}*/

public enum Role {
    Admin, User, None;

    @Override
    public String toString() {
        return super.toString();
    }
}
