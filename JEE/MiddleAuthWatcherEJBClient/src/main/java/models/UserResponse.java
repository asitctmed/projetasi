package models;

import java.io.Serializable;

public class UserResponse implements Serializable {

    private String login;
    private boolean validAuth = false;
    private Role role;
    private String pwd;

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
//    public UserResponse(UserModel userModel, boolean val) {
//        this.login = userModel.getLogin();
//        this.role = userModel.getRole();
//        this.validAuth = val;
//    }

    public String getLogin() {
        return login;
    }

    public boolean isValidAuth() {
        return validAuth;
    }

    public Role getRole() {
        return role;
    }

    public void setRole() { this.role = role; }


    public void all(String login, Role role) {
        this.validAuth = true;
        this.role = role;
        this.login = login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setValidAuth(boolean validAuth) {
        this.validAuth = validAuth;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
