package models;

import java.io.Serializable;

public class UserDTO implements Serializable {
    private String login;
    private String pwd;
    private Role role;

    public Role getRole() { return role; }

    public void setRole(Role role) { this.role = role; }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

}
