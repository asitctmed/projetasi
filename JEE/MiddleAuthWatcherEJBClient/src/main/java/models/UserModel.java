package models;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="Users")
@NamedQuery(name = "Users.byLogin", query = "SELECT u FROM UserModel u WHERE u.login = :login")
public class UserModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private int id;

    @Column(name = "login")
    private String login;

    @Column(name = "pwd")
    private String pwd;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "surName")
    private String surName;

    @Column(name = "role")
    @Enumerated (EnumType.STRING)
    private Role role;

//    public UserModel(String log, String pass) {
//        this.login = log;
//        this.pwd = pass;
//    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastname(String lastname) {
        this.lastName = lastname;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}

