package fr.cpe.rest;

import models.UserDTO;
import models.UserModel;
import models.UserResponse;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/WatcherAuth")

public interface WatcherAuthService {

//    @GET
//    String hello();

//    @POST
//    @Produces(MediaType.APPLICATION_JSON)
//    @Consumes(MediaType.APPLICATION_JSON)
//    UserResponse doPost(UserDTO userDTO);
//
//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    UserResponse get();

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    UserResponse post(UserDTO user);

}
