package fr.cpe.impl;

import ejb.MessageReceiverSyncLocal;
import ejb.MessageSenderLocal;
import models.UserDTO;
import models.UserResponse;
import fr.cpe.rest.WatcherAuthService;
import models.UserModel;

import javax.inject.Inject;

public class WatcherAuthImpl implements WatcherAuthService {

    private static final long serialVersionUID = 1L;
    // private JmsSender sender;

    @Inject
    MessageSenderLocal sender;
    @Inject
    MessageReceiverSyncLocal receiver;

//    @Override
//    public String hello() {
//        return "Hello watcherAuthService (REST)";
//    }
//
//    @Override
//    public UserResponse doPost(UserDTO userDTO) {
//        UserModel user = new UserModel();
//        boolean validAuth = false;
//
//        user.setLogin("thomas");
//        user.setPwd("cordier");
//        user.setRole("Admin");
//
//        if (userDTO.getLogin().equals(user.getLogin())) {
//            if (userDTO.getPwd().equals(user.getPwd())) {
//                validAuth = true;
//            }
//        }
//        UserResponse response = new UserResponse(user, validAuth);
//        return response;
//    }


    @Override
    public UserResponse post(UserDTO user){
        this.sender.sendMessage(user);

        UserResponse userResponse = new UserResponse();

        //userResponse.all(user.getLogin(),user.getRole());

        userResponse = this.receiver.receiveMessage();


        return userResponse;
    }
}

