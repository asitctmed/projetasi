package ejb;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.*;


import ejb.MessageReceiverSyncLocal;
import models.UserModel;
import models.UserResponse;

@Stateless
public class MessageReceiverSync implements MessageReceiverSyncLocal {

    @Inject
    JMSContext context;

    @Resource(mappedName = "java:/jms/queue/watcherqueue")
    Queue queue;

    public UserResponse receiveMessage() {
        Message message = context.createConsumer(queue).receive(10000);

        try {

            if (message !=null)
            {return (UserResponse) ((ObjectMessage) message).getObject();}

        } catch (JMSException e) {
            e.printStackTrace();
        }

        return null;
    }
}